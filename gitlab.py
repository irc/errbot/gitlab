from errbot import BotPlugin, botcmd, arg_botcmd, ValidationException

import gitlab

CI_EMOJIS = {
    'created': '✨',
    'waiting_for_resource': '⏳',
    'preparing': '⏳',
    'pending': '⏳',
    'running': '⏳',
    'success': '✔️',
    'failed': '❌',
    'canceled': '🚫',
    'skipped': '🙃',
    'manual': '🧍',
    'scheduled': '📅'
}


class Gitlab(BotPlugin):
    """
    Commands for interacting with the gitlab API
    """

    def activate(self):
        """
        Triggers on plugin activation

        You should delete it if you're not using it to override any default behaviour
        """
        super(Gitlab, self).activate()

    def deactivate(self):
        """
        Triggers on plugin deactivation

        You should delete it if you're not using it to override any default behaviour
        """
        super(Gitlab, self).deactivate()

    def get_configuration_template(self):
        """
        Defines the configuration structure this plugin supports

        You should delete it if your plugin doesn't use any configuration like this
        """
        return {'gitlab_url': "https://gitlab.com",
                'access_token': "someUserTokenHere"}

    def check_configuration(self, configuration):
        """
        Triggers when the configuration is checked, shortly before activation

        Raise a errbot.utils.ValidationException in case of an error

        You should delete it if you're not using it to override any default behaviour
        """
        super(Gitlab, self).check_configuration(configuration)

    def _getGitlab(self):
        if not self.config:
            raise ValidationException("configuration is not set")
        return gitlab.Gitlab(self.config['gitlab_url'], self.config['access_token'])

    @botcmd
    def projects(self, message, args):
        """Display a list of gitlab projects"""
        gl = self._getGitlab()
        projects = gl.projects.list(visibility='public')
        return " ".join([x.path_with_namespace for x in projects])

    @arg_botcmd('query', type=str)
    def projects_search(self, message, query):
        """Display a list of gitlab projects"""
        gl = self._getGitlab()
        projects = gl.projects.list(search=query, visibility='public')
        if not projects:
            return "sorry, no projects found"
        else:
            return ", ".join(["{ns} - {desc}".format(ns=x.path_with_namespace, desc=x.description) for x in projects])

    @arg_botcmd('name', type=str)
    def project(self, message, name):
        """Display infomation about a spesific project"""
        gl = self._getGitlab()
        project = gl.projects.get(name)
        self.send_card(title=project.name,
                       body=project.description,
                       thumbnail=project.avatar_url,
                       link=project.web_url,
                       in_reply_to=message
                       )

    @arg_botcmd('project', type=str)
    def pipelines(self, message, project):
        """Get the last 5 pipeline status for a given gitlab project"""
        gl = self._getGitlab()
        try:
            project = gl.projects.get(project)

            pipelines = []
            for pipeline in project.pipelines.list()[:5]:
                pipelines.append(
                       "* {1} [{0.id}]({0.web_url}) - {0.status}, by {0.source}".format(
                           pipeline, CI_EMOJIS[pipeline.status]))
            return "\n".join(pipelines)
        except Exception:
            return "Project *{}* did not exist".format(project)

    def _get_project(self, project, pipeline_id):
        if pipeline_id:
            return project.pipelines.get(pipeline_id)
        else:
            return project.pipelines.list()[0]

    @arg_botcmd('--pipeline', type=int, dest='pipeline')
    @arg_botcmd('project_id', type=str)
    def retry(self, message, project_id, pipeline=None):
        gl = self._getGitlab()
        try:
            project = gl.projects.get(project_id)
            pipeline = self._get_project(project, pipeline)

            if pipeline.status == "failed":
                pipeline.retry()
                return "retry request sent"
            else:
                return "that pipeline didn't fail, refusing to retry"

        except Exception as e:
            return "Project *{}* did not exist: {}".format(project_id, e)

    @arg_botcmd('project_id', type=str)
    @arg_botcmd('--pipeline', type=int, dest='pipeline')
    def jobs(self, message, project_id, pipeline=None):
        gl = self._getGitlab()
        try:
            project = gl.projects.get(project_id)
            pipeline = self._get_project(project, pipeline)

            jobs = []
            for job in pipeline.jobs.list():
                jobs.append("* {1} [{0.name}]({0.web_url}) - {0.status} ".format(job, CI_EMOJIS[job.status]))
            return "\n".join(jobs)

        except Exception as e:
            return "Project *{}* did not exist: {}".format(project_id, e)

    @botcmd
    def runners(self, message, args):
        """Print a list of currently online runners"""
        gl = self._getGitlab()
        runners = gl.runners.all(scope="online")
        return ", ".join(["{name}".format(name=x.description) for x in runners])

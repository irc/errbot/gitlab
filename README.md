# An errbot plugin for Gitlab

## How do I install it?
```
!repos install https://git.fossgalaxy.com/errbot/gitlab.git
```

## What libraries does it require?
* python-gitlab